<pre>
<?php

$var="bitm";
$arr=serialize(array("Sajeda","Yeasmin"));
$a=0;
$float_val="3.1678bitm";
$x = array ('a' => 'apple', 'b' => 'banana', 'c' => array ('x', 'y', 'z'));

echo "Printing the floating value \$float_val";
echo floatval($float_val);
echo "</br>";

echo "Printing the all the information of a variable ";
var_dump($var);
//echo "</br>";

echo "Checking if a variable is empty: \$a=";
echo empty($a);
echo "</br>";

echo "Checking if a variable is a array: \$arr=";
echo is_array ($arr);
echo "</br>";

if (isset($var)) {
    echo "Checking if a variable is set to a value ";
}
echo "</br>";

echo "displays information about a variable in a way that's readable by humans.";
print_r($x);
echo "</br>";

echo "Serializing an array </br>";
echo $arr;
echo "</br>";

echo "Unserializing an array </br>";
$var1 = unserialize($arr);
var_dump($var1);
echo "</br>";

echo "Gets structured information about the given variable</br>";
var_export($arr);
echo "</br>";

echo "Returns the type of the PHP variable</br>";
echo gettype($x);
echo "</br>";

echo "Finds whether the given variable is a boolean.";
$p=false;
echo is_bool($p);
echo "</br>";

echo "Finds whether the type of the given variable is float.   ";
var_dump(is_float($float_val));
echo "</br>";

echo "Finds whether the type given variable is string.   ";
var_dump(is_string($float_val));
echo "</br>";

echo "Finds whether the type of the given variable is integer.   ";
var_dump(is_int($float_val));
echo "</br>";

echo "Returns the boolean value of var.   ";
var_dump(boolval($a));
echo "</br>";


?>
</pre>